# Install 
    $sh build.sh
# Clear
    $sh clear.sh

# Test
    http://localhost:80/sessionsync
    http://localhost:81/sessionsync
    http://localhost:82/sessionsync

# Custom

    1. Si solo se desea crear los contenedores comentar la linea 3 con el caracter # del archivo build.sh

    2. Si solo se desea parar los contenedores sin borrarlos, comentar la linea 3 con el caracter # del archivo clear.sh

    3. Si se desea hacer lo inverso proceder a comentar las lineas que interese.